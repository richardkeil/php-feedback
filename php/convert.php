<?php
# Convert something to string
# 
# @param	$data, data to convert
# @return	string
function convertToString($data) {
	$string = strtolower(strval($data));
	return $string;
}

# Convert something to an array
# 
# @param	$data, data to convert
# @return	array
function convertToArray($data) {
	$array = (array) $data;
	return $array;
}
?>