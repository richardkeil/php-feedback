<?php
include 'convert.php';


# Get backend form in html-syntax
# 
# @param	string $mode, mode for form ('view', 'write')
# @param	array $entries_array, all entries
# @return	string
function renderBackendForm($mode = 'view') {
	$mode = convertToString($mode);
	$form_str = '';
	
	if ($mode == 'write') {			  
		# create html for new entry
		$form_str = $form_str.
					'<h2>Add new entry</h2>
					<form action="index.php?mode=view" method="post">
						<textarea name="message" placeholder="Hey, this is a test message!"></textarea>
						<button name="cancel">Cancel</button>
						<button name="submit" onclick="this.form.submit()">Submit!</button>
					</form>';			
	} elseif ($mode == 'view') {			
		# create html for entry view			
		$form_str = $form_str.
					'<h2>All entries</h2>
					<form action="index.php?mode=write" method="post">		 
						<button name="write">Write new entry</button> 
					</form>';	   
	}		
	return $form_str;
}
?>