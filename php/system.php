<?php
session_start();

# Get final form in html-syntax
# 
# @return	string
function runSystem() {
	include 'render.php';
	
	$mode = '';
	
	# new entry?
	if (isset($_POST['submit'])) {
		submitEntry($_POST['message']);
		$_GET['mode'] = 'view';
	}
	
	# has mode been set?
	if (isset($_GET['mode'])) {
		$mode = $_GET['mode'];
	} else {
		$mode = 'view';	
	}
	
	$entries_array = getEntries();
	
	return renderBackendForm($mode, $entries_array);
}

# Save a new entry in xml-file
# 
# @param	string $message, message of entry
# @return	void
function submitEntry($message) {
	global $entries_path;
	if (file_exists($entries_path)) {
		$file = simplexml_load_file($entries_path);
		
		$entry = $file->addChild('entry');
		$entry->addAttribute('id', $file->count()+1);
		$author = $entry->addChild('author', 'Crispi');
		$msg = $entry->addChild('message', $message);

		$file->asXml($entries_path);		
	} else {
		exit($entries_path.' not found!');
	}
}


# Get all entries from xml-file in array
# 
# @return	array
function getEntries() {
	$entries = array();
	
	global $entries_path;
	if (file_exists($entries_path)) {
		$file = simplexml_load_file($entries_path);
		foreach ($file as $entry) {
			$entries[] = array(	'author' => $entry->author,
								'message' => $entry->message);
		}
	} else {
		exit($entries_path.' not found!');
	}
		
	return $entries;
}
?>