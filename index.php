<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Feedback Form</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="bower_components/normalize.css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
	</head>
	<body>
		<!--[if lt IE 8]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
		<h1>Feedback Form</h1>
		<?php
			include 'php/system.php';
			echo runSystem();
		?>
	</body>
</html>